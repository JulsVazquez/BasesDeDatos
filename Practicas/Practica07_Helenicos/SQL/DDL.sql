/*
Script para la creacion de la Base de Datos
del caso de uso para la empresa Nixut.
*/

--Creacion del esquema...
DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA public;

--Creacion de las entidades...

--SUPERENTIDAD PERSONA.
CREATE TABLE Persona(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	FechaDeNacimiento DATE NOT NULL,
	Genero CHAR(1) NOT NULL CHECK (Genero in ('M','F')),
	Nombre VARCHAR(50) CHECK (Nombre <> ''),
	ApellidoPaterno VARCHAR(50) CHECK (ApellidoPaterno <> ''),
	ApellidoMaterno VARCHAR(50) CHECK (ApellidoMaterno <> ''),
	NumeroDeCalle int CHECK (NumeroDeCalle between 0 and 1000),
	NombreDeCalle VARCHAR(50) CHECK (NombreDeCalle <> ''),
	Municipio VARCHAR(50) CHECK (Municipio <> ''),
	CodigoPostal CHAR(5) CHECK (CodigoPostal SIMILAR TO '[0-9]*'),
	--CodigoPostal int  CHECK (CodigoPostal between 0 and 99999), --Alternativa para CP.
	Estado VARCHAR(50) CHECK (Estado <> ''),
	PRIMARY KEY(CURP)
);

COMMENT ON TABLE Persona IS 'Tabla que contiene la información de una Persona.';
COMMENT ON COLUMN Persona.CURP IS 'El CURP que contiene un total de 18 carácteres.';
COMMENT ON COLUMN Persona.FechaDeNacimiento IS 'El día que nació la persona.';
COMMENT ON COLUMN Persona.Genero IS 'El género de la Persona, podemos elegir entre Masculino o Feminino';
COMMENT ON COLUMN Persona.Nombre IS 'El nombre de pila de la persona.';
COMMENT ON COLUMN Persona.ApellidoPaterno IS 'El apellido paterno.';
COMMENT ON COLUMN Persona.ApellidoMaterno IS 'El apellido materno.';
COMMENT ON COLUMN Persona.NumeroDeCalle IS 'El número de la calle donde vive la persona.';
COMMENT ON COLUMN Persona.NombreDeCalle IS 'El nombre de la calle donde vive la persona.';
COMMENT ON COLUMN Persona.Municipio IS 'El municipio de residencia de la persona.';
COMMENT ON COLUMN Persona.CodigoPostal IS 'El código postal de residencia de la persona.';
COMMENT ON COLUMN Persona.Estado IS 'El estado de residencia de la persona.';


--REPARTIDOR (subentidad de Persona).
CREATE TABLE Repartidor(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Salario int CHECK (Salario > 0),
	Calificacion decimal(4,2) CHECK (Calificacion between 0 and 10),
	HorarioDeEntrada int CHECK (HorarioDeEntrada between 0 and 86400),
	HorarioDeSalida int CHECK (HorarioDeSalida between 0 and 86400),
	FOREIGN KEY(CURP) REFERENCES Persona(CURP) --Referencia a Persona.
);

alter table Repartidor drop constraint CURP;
-- Politica de cascada.
alter table Repartidor add constraint CURP foreign key(CURP) references Persona(CURP)
on update cascade on delete cascade;


COMMENT ON TABLE Repartidor IS 'Tabla que contiene a los repartidores, es una subentidad de Persona';
COMMENT ON COLUMN Repartidor.CURP IS 'Curp del repartidor';
COMMENT ON COLUMN Repartidor.Salario IS 'El salario que recibe el repartido';
COMMENT ON COLUMN Repartidor.Calificacion IS 'La calificación total del repartidor';
COMMENT ON COLUMN Repartidor.HorarioDeEntrada IS 'El horario de entrada de nuestro repartidor';
COMMENT ON COLUMN Repartidor.HorarioDeSalida IS 'El horario de salida de nuestro repartidor';

--CLIENTE (subentidad de Persona).
CREATE TABLE Cliente(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Contraseña VARCHAR(30) CHECK (Contraseña <> ''),
	Correo VARCHAR(30) CHECK(Correo <> '' and Correo like '_%@_%._%'),
	PuntosAcumulados int,
	FOREIGN KEY(CURP) REFERENCES Persona(CURP) --Referencia a Persona.
);

alter table Cliente drop constraint CURP
-- Politica de cascada.
alter table Cliente add constraint CURP foreign key (CURP) references Persona(CURP)
on update cascade on delete cascade;

COMMENT ON TABLE Cliente IS 'Tabla que contiene a todos los clientes, es una subentidad de Persona';
COMMENT ON COLUMN Cliente.CURP IS 'El CURP del Cliente';
COMMENT ON COLUMN Cliente.Contraseña IS 'La contraseña del cliente';
COMMENT ON COLUMN Cliente.correo IS 'El correo electrónico del cliente';
COMMENT ON COLUMN Cliente.PuntosAcumulados IS 'El total de puntos acumulados que tiene nuestro cliente';

--ENTIDAD PROVEEDOR.
CREATE TABLE Proveedor(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13) UNIQUE,
	RazonSocial VARCHAR(50) CHECK (RazonSocial <> ''),
	NumeroDeCalle int CHECK (NumeroDeCalle between 0 and 1000),
	NombreDeCalle VARCHAR(50) CHECK (NombreDeCalle <> ''),
	Municipio VARCHAR(50) CHECK (Municipio <> ''),
	CodigoPostal CHAR(5) CHECK (CodigoPostal SIMILAR TO '[0-9]*'),
	Estado VARCHAR(50) CHECK (Estado <> ''),
	PRIMARY KEY(RFC)
);

COMMENT ON TABLE Proveedor IS 'Tabla que contiene a los proveedores.';
COMMENT ON COLUMN Proveedor.RFC IS 'El RFC del proveedor.';
COMMENT ON COLUMN  Proveedor.RazonSocial IS 'La razón social del proveedor.';
COMMENT ON COLUMN Proveedor.NumeroDeCalle IS 'El numero de la calle donde vive nuestro proveedor';
COMMENT ON COLUMN Proveedor.NombreDeCalle IS 'El nombre de la calle donde vive nuestro proveedor';
COMMENT ON COLUMN Proveedor.Municipio IS 'El municipio de residencia del proveedor.';
COMMENT ON COLUMN Proveedor.CodigoPostal IS 'El código postal de nuestro proveedor.';
COMMENT ON COLUMN Proveedor.Estado IS 'El estado de residencia de nuestro proveedor.';

--Telefono de Contacto (multivaluado) de los proveedores.
CREATE TABLE TelefonoProveedor(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13) UNIQUE,
	TelefonoDeContacto CHAR(10) CHECK (TelefonoDeContacto SIMILAR TO '[0-9]*'),
	PRIMARY KEY(RFC, TelefonoDeContacto),
	FOREIGN KEY(RFC) REFERENCES Proveedor(RFC) --Referencia a Proveedor.
);

alter table TelefonoProveedor drop constraint RFC;
--Politica de cascada
alter table TelefonoProveedor add constraint RFC foreign key (RFC) references Proveedor(RFC)
on update cascade on delete cascade;

COMMENT ON TABLE TelefonoProveedor IS 'Tabla que contiene el Telefono del Proveedor.';
COMMENT ON COLUMN TelefonoProveedor.RFC IS 'El RFC relacionado al Telefono del Proveedor.';
COMMENT ON COLUMN TelefonoProveedor.TelefonoDeContacto 'El telefono de contacto de nuestro Proveedor.';

--ENTIDAD CATEGORIA.
CREATE TABLE Categoria(
	ID VARCHAR(30) NOT NULL UNIQUE,
	Nombre VARCHAR(30) CHECK(Nombre <> ''),
	Descripcion VARCHAR(300) CHECK(Descripcion <> ''),
	PRIMARY KEY(ID)
);

COMMENT ON TABLE Categoria IS 'Tabla de la categoria de los productos.';
COMMENT ON COLUMN Categoria.ID IS 'EL ID que corresponde a la categoria.';
COMMENT ON COLUMN Categoria.Nombre IS 'El nombre de la categoria.';
COMMENT ON COLUMN Categoria.Descripcion IS 'La descripción de la categoria.';

--ENTIDAD PRODUCTO.
CREATE TABLE Producto(
	ID VARCHAR(30) NOT NULL UNIQUE,
	Nombre VARCHAR(50) CHECK(Nombre <> ''),
	Precio decimal(8,2) CHECK (Precio > 0),
	Descripcion VARCHAR(300) CHECK(Descripcion <> ''),
	ImagenAsociada VARCHAR(50) CHECK (ImagenAsociada <> ''),
	Descuento decimal(3,2) CHECK (Descuento between 0 and 1),
	--UnidadesDisp int CHECK (UnidadesDisp between 0 and 99999), --Atributo calculado.
	PRIMARY KEY(ID),
	FOREIGN KEY(ID) REFERENCES Categoria(ID)
);

alter table Producto drop constraint ID;
--Politica de cascada
alter table Producto add constraint ID foreign key(ID) references Categoria(ID)
on update cascade on delete cascade;

COMMENT ON TABLE Producto IS 'Tabla de los productos.';
COMMENT ON COLUMN Producto.ID IS 'El ID del producto.';
COMMENT ON COLUMN Producto.Nombre IS 'El nombre del producto.';
COMMENT ON COLUMN Producto.Precio IS 'El precio del producto.';
COMMENT ON COLUMN Producto.Descripcion IS 'La descripción de nuestro producto.';
COMMENT ON COLUMN Producto.ImagenAsociada IS 'La imagen asociada a nuestro producto.';
COMMENT ON COLUMN Producto.Descuento IS 'El descuento que tenga el producto.';

--ENTIDAD PEDIDO.
CREATE TABLE Pedido(
	Numero int NOT NULL CHECK (Numero between 0 and 999999) UNIQUE,
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	PRIMARY KEY(Numero),
	FOREIGN KEY(CURP) REFERENCES Cliente(CURP) --Referencia a Cliente.
);

alter table Pedido drop constraint CURP;
--Politica en cascada
alter table Pedido add constraint CURP foreign key(CURP) references Cliente(CURP)
on update cascade on delete cascade;

COMMENT ON TABLE Pedido IS 'Tabla de los pedidos.';
COMMENT ON COLUMN Pedido.Numero IS 'El numero que corresponde al pedido.';
COMMENT ON COLUMN Pedido.CURP IS 'El CURP que corresponde a la persona que realizo el pedido.';

--ENTIDAD ENVIO.
CREATE TABLE Envio(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Numero int NOT NULL CHECK(Numero between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	PRIMARY KEY(Numero, CURP),
	FOREIGN KEY(CURP) REFERENCES Repartidor(CURP) --Referencia a Repartidor.
);

alter table Envio drop constraint CURP;
--Politica en cascada
alter table Envio add constraint CURP foreign key(CURP) references Repartidor(CURP)
on update cascade on delete cascade;

COMMENT ON TABLE Envio IS 'Tabla que contiene los envios.';
COMMENT ON COLUMN Envio.CURP IS 'El CURP que corresponde al envío del producto.';
COMMENT ON COLUMN Envio.Numero IS 'El número del envío del producto.';
COMMENT ON COLUMN Envio.FueCompletado IS 'El valor de verdadero o falso de acuerdo a si se envío o no el pedido.';

--NORMAL (subentidad de Envio).
CREATE TABLE Normal(
	--La CURP está en Envio.
	NumeroDeEnvio int NOT NULL CHECK(NumeroDeEnvio between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	TiempoDeDemanda INT NOT NULL CHECK(TiempoDeDemanda > 0),
	PRIMARY KEY(NumeroDeEnvio),
	FOREIGN KEY(NumeroDeEnvio) REFERENCES Envio(Numero) --Referencia a Envio.

);

alter table Normal drop constraint NumeroDeEnvio;
--Politica en cascada
alter table Normal add constraint NumeroDeEnvio foreign key(NumeroDeEnvio) references Envio(Numero)
on update cascade on delete cascade;

COMMENT ON TABLE Normal IS 'Tabla que contiene los envios normales, es una subentidad de envío.';
COMMENT ON COLUMN Normal.NumeroDeEnvio IS 'El numero de envío normal que se hizo.';
COMMENT ON COLUMN Normal.FueCompletado IS 'El estado del envío normal si se completo o no.';
COMMENT ON COLUMN Normal.TiempoDeDemando IS 'El tiempo de demanda del envio normal';

--EXPRESS (subentidad de Envio).
CREATE TABLE Express(
	--La CURP está en Envio.
	NumeroDeEnvio int NOT NULL CHECK(NumeroDeEnvio between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	TiempoUsuario INT NOT NULL CHECK(TiempoUsuario > 0),
	Costo INT CHECK(Costo >= 0),
	PRIMARY KEY(NumeroDeEnvio),
	FOREIGN KEY(NumeroDeEnvio) REFERENCES Envio(Numero) --Referencia a Envio.
);

alter table Express drop constraint NumeroDeEnvio;
--Politica en cascada
alter table Express add constraint NumeroDeEnvio foreign key(NumeroDeEnvio) references Envio(Numero)
on update cascade on delete cascade;

COMMENT ON TABLE Express IS 'Tabla que contiene los envíos express, es una subentidad de envío.';
COMMENT ON COLUMN Express.NumeroDeEnvio IS 'El número de envío express que se realizó.';
COMMENT ON COLUMN Express.FueCompletado IS 'El estado del envío express si se completo o no.';
COMMENT ON COLUMN Express.TiempoUsuario IS 'El tiempo que tomará para el usuario en llegar el envío.';
COMMENT ON COLUMN Express.Costo IS 'El costo total del envío express.';

--ENTIDAD TARJETA.
CREATE TABLE Tarjeta(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	Vencimiento DATE NOT NULL,
	NombreDeTitular VARCHAR(60) CHECK (NombreDeTitular <> ''),
	CVV CHAR(3) CHECK (CVV SIMILAR TO '[0-9]*'),
	PRIMARY KEY(NumDeTarjeta),
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	FOREIGN KEY(CURP) references Pedido(CURP) --Referencia a Pedido.
);

alter table Tarjeta drop constraint CURP;
--Politica en cascada
alter table Tarjeta add constraint CURP foreign key(CURP) references Pedido(CURP)
on update cascade on delete cascade;

COMMENT ON TABLE Tarjeta IS 'Tabla que contiene las tarjetas con las que se compro algo.';
COMMENT ON COLUMN Tarjeta.NumDeTarjeta IS 'El número que corresponde a la tarjeta.';
COMMENT ON COLUMN Tarjeta.Vencimiento IS 'El vencimiento de la tarjeta.';
COMMENT ON COLUMN Tarjeta.NombreDeTitular IS 'El nombre del titular de la tarjeta.';
COMMENT ON COLUMN Tarjeta.CVV IS 'El CVV de la tarjeta.';
COMMENT ON COLUMN Tarjeta.CURP IS 'EL CURP al que corresponde la Tarjeta.';

--CREDITO (subentidad de Tarjeta).
CREATE TABLE Credito(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	FOREIGN KEY(NumDeTarjeta) references Tarjeta(NumDeTarjeta) --Referencia a Tarjeta.
);

alter table Credito drop constraint NumDeTarjeta;
--Politica en cascada
alter table Credito add constraint NumDeTarjeta foreign key(NumDeTarjeta) references Tarjeta(NumDeTarjeta)
on update cascade on delete cascade;

COMMENT ON TABLE Credito IS 'Tabla que contiene todas las tarjetas de crédito.';
COMMENT ON COLUMN Credito.NumDeTarjeta IS 'El número de la tarjeta de crédito.';

--DEBITO (subentidad de Tarjeta).
CREATE TABLE Debito(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	FOREIGN KEY(NumDeTarjeta) references Tarjeta(NumDeTarjeta) --Referencia a Tarjeta.
);

alter table Debito drop constraint NumDeTarjeta;
--Politica en cascada
alter table Debito add constraint NumDeTarjeta foreign key(NumDeTarjeta) references Tarjeta(NumDeTarjeta)
on update cascade on delete cascade;


COMMENT ON TABLE Debito IS 'Tabla que contiene todas las tarjetas de débito.';
COMMENT ON COLUMN Debito.NumDeTarjeta IS 'El número de la tarjeta de débito.';

--RELACION VENDER.
CREATE TABLE Vender(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13),
	ID VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY(RFC, ID),
	FOREIGN KEY(RFC) REFERENCES Proveedor(RFC),
	FOREIGN KEY(ID) REFERENCES Producto(ID)
);

alter table Vender drop constraint RFC;
alter table Vender drop constraint ID;
--Politica en Cascada
alter table Vender add constraint RFC foreign key(RFC) references Proveedor(RFC)
on update cascade on delete cascade;

alter table Vender add constraint ID foreign key(ID) references Producto(ID)
on update cascade on delete cascade;

COMMENT ON TABLE Vender IS 'Tabla que contiene lo que se puede vender.';
COMMENT ON COLUMN Vender.RFC IS 'Contiene el RFC asociado al vendedor que puede hacer la venta.';
COMMENT ON COLUMN Vender.ID IS 'Contiene el ID asociado a la venta que se realizó.';


