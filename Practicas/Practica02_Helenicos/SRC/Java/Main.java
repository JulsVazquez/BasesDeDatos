/**
 * Clase principal.
 */
public class Main {

	/**
	 * Método principal (e inicial).
	 * @param args Argumentos del programa.
	 */
	public static void main(String[] args) {
		Interfaz io = new Interfaz();
		io.menuPrincipal();
	}

}