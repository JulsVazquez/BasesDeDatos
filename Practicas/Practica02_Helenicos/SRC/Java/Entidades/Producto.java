package Entidades;

/**
 * Tipo de entidad Producto.
 */
public class Producto extends TipoEntidad {

	// 0  id;
	// 1  precio;
	// 2  unidadesDisponibles;
	// 3  descripción;
	// 4  archivoImagen;
	// 5  descuento;
	// 6  categoría;

	/**
	 * Constructor de un Producto.
	 * @param datos Arreglo con los atributos
	 * del producto a crear.
	 */
	public Producto(String[] datos) {
		super(datos);
	}

	/**
	 * Condiciona los datos/atributos.
	 * @throws IllegalArgumentException Si algún
	 * dato no cumple las condiciones que debería.
	 */
 	protected void validarDatos(String[] datos) throws IllegalArgumentException {
		if (datos.length != 7)
			throw new IllegalArgumentException("Dentro del archivo .csv, cada producto "
											+ "debe contar exactamente con 7 atributos, "
											+ "aunque hayan algunos vacíos (ver README).");

		try {
			Integer.parseInt(datos[0]);
		 	Double.parseDouble(datos[1]);
			Integer.parseInt(datos[2]);
			Double.parseDouble(datos[5]);
		} catch (Exception e) {
			throw new IllegalArgumentException("El ID y/o precio y/o unidades disponibles "
											+ "y/o descuento debe(n) representarse con "
											+ "un valor numérico.");
		}

	}

	/**
	 * Devuelve la categoría a la cual el
	 * producto pertenece.
	 * @return Entero que representa el
	 * ID de la categoría a la que pertenece.
	 */
	public int getCategoria() {
		return Integer.parseInt(datos[6]);
	}

}