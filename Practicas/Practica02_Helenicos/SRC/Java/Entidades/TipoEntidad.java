package Entidades;

/**
 * Un Tipo de Entidad es algo sobre lo
 * que queremos almacenar datos. Esta
 * clase abstracta da la pauta para las
 * demás clases que representarán tipos
 * de entidad.
 */
public abstract class TipoEntidad {

	protected String[] datos;

	/**
	 * Constructor de una instancia de algún
	 * tipo de entidad.
	 * @param datos Arreglo con los atributos
	 * de la instancia a crear.
	 */
	public TipoEntidad(String[] datos) {
		setDatos(datos);
	}

	/**
	 * Setter de los atributos de la instancia.
	 * @param datos Arreglo con los nuevos
	 * atributos.
	 */
	public void setDatos(String[] datos) {
		validarDatos(datos);
		this.datos = datos;
	}

	/**
	 * Getter del identificador único de la instancia.
	 * @return Identificador único de la instancia.
	 */
	public String getID() {
		return datos[0];
	}

	/**
	 * Representación en cadena de la instancia.
	 * @return Representación en cadena de la instancia.
	 */
	public String toString() {
		String s = "";
		for (int i = 0; i < datos.length; i++)
			s += datos[i] + ",";
		s = s.substring(0, s.length()-1); // Elimina la última coma.
		return s;
	}

	/**
	 * Cada tipo de entidad describe cómo son sus
	 * datos; que sí acepta, qué no, etc.
	 * @param datos Arreglo de datos a validar.
	 * @throws IllegalArgumentException Si algún
	 * dato no cumple las condiciones que debería.
	 */
	protected abstract void validarDatos(String[] datos) throws IllegalArgumentException;

}