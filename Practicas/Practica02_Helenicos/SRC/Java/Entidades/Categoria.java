package Entidades;

/**
 * Tipo de entidad Categoria.
 */
public class Categoria extends TipoEntidad {

	// 0  id;
	// 1  nombre;
	// 2  descripción;

	/**
	 * Constructor de una Categoria.
	 * @param datos Arreglo con los atributos
	 * de la categoría a crear.
	 */
	public Categoria(String[] datos) {
		super(datos);
	}

	/**
	 * Condiciona los datos/atributos.
	 * @throws IllegalArgumentException Si algún
	 * dato no cumple las condiciones que debería.
	 */
	protected void validarDatos(String[] datos) throws IllegalArgumentException {
		if (datos.length != 3)
			throw new IllegalArgumentException("Dentro del archivo .csv, cada categoría "
											+ "debe contar exactamente con 3 atributos, "
											+ "aunque hayan algunos vacíos (ver README).");

		try {
			Integer.parseInt(datos[0]);
		} catch (Exception e) {
			throw new IllegalArgumentException("El ID debe representarse con "
											+ "un valor numérico.");
		}

	}

}