package Entidades;

/**
 * Tipo de entidad Proveedor.
 */
public class Proveedor extends TipoEntidad {

	// 0  rfc;
	// 1  razónSocial;
	// 2  calle;
	// 3  númeroCalle;
	// 4  municipio;
	// 5  códigoPostal;
	// 6  estado;
	// 7  teléfonosContacto (más de uno);

	/**
	 * Constructor de un Proveedor.
	 * @param datos Arreglo con los atributos
	 * del proveedor a crear.
	 */
	public Proveedor(String[] datos) {
		super(datos);
	}

	/**
	 * Condiciona los datos/atributos.
	 * @throws IllegalArgumentException Si algún
	 * dato no cumple las condiciones que debería.
	 */
	protected void validarDatos(String[] datos) throws IllegalArgumentException {
		if (datos.length != 8)
			throw new IllegalArgumentException("Dentro del archivo .csv, cada proveedor "
											+ "debe contar exactamente con 8 atributos, "
											+ "aunque hayan algunos vacíos (ver README).");
	}

}