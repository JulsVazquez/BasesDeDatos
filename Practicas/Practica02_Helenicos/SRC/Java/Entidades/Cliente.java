package Entidades;

import java.time.LocalDate;
import java.time.Period;

/**
 * Tipo de entidad Cliente.
 */
public class Cliente extends TipoEntidad {

	// 0  curp;
	// 1  nombre;
	// 2  apellidoPaterno;
	// 3  apellidoMaterno;
	// 4  calle;
	// 5  númeroCalle;
	// 6  municipio;
	// 7  códigoPostal;
	// 8  estado;
	// 9  género;
	// 10 fechaNacimiento;
	// 11 correoElectronico;
	// 12 contraseña;
	// 13 métodoPago;
	// 14 puntosAcumulados;

	/**
	 * Constructor de un Cliente.
	 * @param datos Arreglo con los atributos
	 * del cliente a crear.
	 */
	public Cliente(String[] datos) {
		super(datos);
	}

	/**
	 * Calcula la edad del cliente.
	 * @return Edad del cliente.
	 */
	public int getEdad() {
		String fecha = datos[10];
		int dia = Integer.parseInt(fecha.substring(0, 2));
		int mes = Integer.parseInt(fecha.substring(3, 5));
		int anio = Integer.parseInt(fecha.substring(6));

		LocalDate fechaNacimiento = LocalDate.of(anio, mes, dia);
		LocalDate hoy = LocalDate.now();

		return Period.between(fechaNacimiento, hoy).getYears();
	}

	/**
	 * Condiciona los datos/atributos.
	 * @throws IllegalArgumentException Si algún
	 * dato no cumple las condiciones que debería.
	 */
	protected void validarDatos(String[] datos) throws IllegalArgumentException {
		if (datos.length != 15)
			throw new IllegalArgumentException("Dentro del archivo .csv, cada cliente "
											+ "debe contar exactamente con 15 atributos, "
											+ "aunque hayan algunos vacíos (ver README).");

		if (!datos[10].substring(2, 3).equals("/")
		|| !datos[10].substring(5, 6).equals("/")
		|| Integer.parseInt(datos[10].substring(3, 5)) > 12) {
			throw new IllegalArgumentException("La fecha de cumpleaños debe llevar el "
											+ "formato DD/MM/AAAA");
		}

	}

}