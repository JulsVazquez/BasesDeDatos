import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;

/**
 * Clase encargada de la lectura
 * y escritura de archivos CSV.
 */
public class ManejadorCSV {

	private File archivo;
	private List<String> lineas = new ArrayList<String>();

	/**
	 * Constructor de un ManejadorCSV. Supone que el delimitador de campos
	 * dentro de una misma línea es una COMA.
	 * @param ruta Ruta donde se ubica el archivo CSV a leer y/o modificar.
	 */
	public ManejadorCSV(String ruta) {
		archivo = new File(ruta);
		parsear();
	}

	/**
	 * Agrega una línea al final del archivo.
	 * @param linea Línea a añadir.
	 */
	public void escribirLinea(String linea) {
        try {
            FileWriter pluma = new FileWriter(archivo, true);
            pluma.write(linea);
            pluma.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
	}

	/**
	 * Vacía el archivo completo.
	 */
	public void vaciar() {
        try {
            FileWriter pluma = new FileWriter(archivo);
            //pluma.write();
            pluma.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
	}

	/**
	 * Traduce el archivo CSV al almacenar cada
	 * una de sus líneas en la variable "lineas".
	 */
	private void parsear() {
		try {
			Scanner scanner = new Scanner(archivo);
			while (scanner.hasNextLine()) {
				lineas.add(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Regresa, en una lista de cadenas, todas
	 * las líneas del archivo.
	 * @return Lista de String's con las líneas
	 * del archivo.
	 */
	public List<String> getLineas() {
		return lineas;
	}
	

}