import java.util.List;
import java.util.ArrayList;
import Entidades.*;

/**
 * Clase encargada de manejar
 * la base de datos.
 */
public class ManejadorBD {

	private String almacen;

	private ManejadorCSV clienteCSV;
	private List<Cliente> clientes = new ArrayList<Cliente>();

	private ManejadorCSV proveedorCSV;
	private List<Proveedor> proveedores = new ArrayList<Proveedor>();

	private ManejadorCSV productoCSV;
	private List<Producto> productos = new ArrayList<Producto>();

	private ManejadorCSV categoriaCSV;
	private List<Categoria> categorias = new ArrayList<Categoria>();

	/**
	 * Constructor de un manejador
	 * de bases de datos.
	 */
	public ManejadorBD() {
		almacen = "../BD/";
		clienteCSV = new ManejadorCSV(almacen + "Cliente.csv");
		proveedorCSV = new ManejadorCSV(almacen + "Proveedor.csv");
		productoCSV = new ManejadorCSV(almacen + "Producto.csv");
		categoriaCSV = new ManejadorCSV(almacen + "Categoría.csv");
		leerBD();
	}


	// DE LOS CLIENTES...

	/**
	 * Agrega un cliente a la BD.
	 * @param linea Cadena con los datos del cliente, simula
	 * la línea del respectivo archivo CSV.
	 * @throws IllegalArgumentException Excepción si un cliente
	 * con la misma CURP ya existe dentro de la BD.
	 */
	public void agregarCliente(String linea) throws IllegalArgumentException {
		String[] datos = linea.split(",");
		if (buscarCliente(datos[0]) != null)
			throw new IllegalArgumentException("Un cliente ya cuenta con esa CURP.");
		clientes.add( new Cliente(datos) );
	}

	/**
	 * Imprime todos los clientes de la BD,
	 * incluyendo su respectiva edad.
	 */
	public void consultarClientes() {
		for (Cliente cliente : clientes)
			System.out.println(cliente + "," + cliente.getEdad() + " años");
	}

	/**
	 * Dada su CURP, busca un cliente en la BD.
	 * @param curp CURP del cliente a buscar.
	 * @return El respectivo Cliente, o null si no hay algún cliente con dicha
	 * CURP dentro de la BD.
	 */
	public Cliente buscarCliente(String curp) {
		for (Cliente cliente : clientes)
			if ( cliente.getID().equals(curp) )
				return cliente;
		return null;
	}

	/**
	 * Actualiza los datos de un cliente dentro de la BD.
	 * @param cliente Cliente cuyos datos serán modificados.
	 * @param linea Cadena con los datos del cliente, simula
	 * la línea del respectivo archivo CSV.
	 */
	public void setCliente(Cliente cliente, String linea) {
		String[] datos = linea.split(",");
		cliente.setDatos(datos);
	}

	/**
	 * Elimina un cliente de la BD.
	 * @param cliente Cliente a ser eliminado.
	 */
	public void eliminarCliente(Cliente cliente) {
		clientes.remove(cliente);
	}

	// DE LOS PROVEEDORES...

	/**
	 * Agrega un proveedor a la BD.
	 * @param linea Cadena con los datos del proveedor, simula
	 * la línea del respectivo archivo CSV.
	 * @throws IllegalArgumentException Excepción si un proveedor
	 * con el mismo RFC ya existe dentro de la BD.
	 */
	public void agregarProveedor(String linea) throws IllegalArgumentException {
		String[] datos = linea.split(",");
		if (buscarProveedor(datos[0]) != null)
			throw new IllegalArgumentException("Un proveedor ya cuenta con ese RFC.");
		proveedores.add( new Proveedor(datos) );
	}

	/**
	 * Imprime todos los proveedores de la BD.
	 */
	public void consultarProveedores() {
		for (Proveedor proveedor : proveedores)
			System.out.println(proveedor);
	}

	/**
	 * Dado su RFC, busca un proveedor en la BD.
	 * @param rfc RFC del proveedor a buscar.
	 * @return El respectivo Proveedor, o null si no hay algún proveedor
	 * con dicho RFC dentro de la BD.
	 */
	public Proveedor buscarProveedor(String rfc) {
		for (Proveedor proveedor : proveedores)
			if ( proveedor.getID().equals(rfc) )
				return proveedor;
		return null;
	}

	/**
	 * Actualiza los datos de un proveedor dentro de la BD.
	 * @param proveedor Proveedor cuyos datos serán modificados.
	 * @param linea Cadena con los datos del proveedor, simula
	 * la línea del respectivo archivo CSV.
	 */
	public void setProveedor(Proveedor proveedor, String linea) {
		String[] datos = linea.split(",");
		proveedor.setDatos(datos);
	}

	/**
	 * Elimina un proveedor de la BD.
	 * @param proveedor Proveedor a ser eliminado.
	 */
	public void eliminarProveedor(Proveedor proveedor) {
		proveedores.remove(proveedor);
	}

	// DE LOS PRODUCTOS...

	/**
	 * Agrega un producto a la BD.
	 * @param linea Cadena con los datos del producto, simula
	 * la línea del respectivo archivo CSV.
	 * @throws IllegalArgumentException Excepción si un producto
	 * con el mismo ID ya existe dentro de la BD.
	 */
	public void agregarProducto(String linea) throws IllegalArgumentException {
		String[] datos = linea.split(",");
		if (buscarProducto(datos[0]) != null)
			throw new IllegalArgumentException("Un producto ya cuenta con ese ID.");
		productos.add( new Producto(datos) );
	}

	/**
	 * Imprime todos los productos de la BD.
	 */
	public void consultarProductos() {
		for (Producto producto : productos)
			System.out.println(producto);
	}

	/**
	 * Dado su ID, busca un producto en la BD.
	 * @param id ID del producto a buscar.
	 * @return El respectivo Producto, o null si no hay algún producto
	 * con dicho ID dentro de la BD.
	 */
	public Producto buscarProducto(String id) {
		for (Producto producto : productos)
			if ( producto.getID().equals(id) )
				return producto;
		return null;
	}

	/**
	 * Actualiza los datos de un producto dentro de la BD.
	 * @param producto Producto cuyos datos serán modificados.
	 * @param linea Cadena con los datos del producto, simula
	 * la línea del respectivo archivo CSV.
	 */
	public void setProducto(Producto producto, String linea) {
		String[] datos = linea.split(",");
		producto.setDatos(datos);
	}

	/**
	 * Elimina un producto de la BD.
	 * @param producto Producto a ser eliminado.
	 */
	public void eliminarProducto(Producto producto) {
		productos.remove(producto);
	}

	// DE LAS CATEGORÍAS...

	/**
	 * Agrega una categoría a la BD.
	 * @param linea Cadena con los datos de la categoría, simula
	 * la línea del respectivo archivo CSV.
	 * @throws IllegalArgumentException Excepción si una categoría
	 * con el mismo ID ya existe dentro de la BD.
	 */
	public void agregarCategoria(String linea) throws IllegalArgumentException {
		String[] datos = linea.split(",");
		if (buscarCategoria(datos[0]) != null)
			throw new IllegalArgumentException("Una categoría ya cuenta con ese ID.");
		categorias.add( new Categoria(datos) );
	}

	/**
	 * Imprime todas las categorías de la BD,
	 * incluyendo cuántos productos tiene.
	 */
	public void consultarCategorias() {
		for (Categoria categoria : categorias){
			int n = 0;
			for (Producto p : productos)
				if (p.getCategoria() == Integer.parseInt(categoria.getID()))
					n++;
			System.out.println(categoria + "," + n + " productos");
		}
	}

	/**
	 * Dado su ID, busca una categoría en la BD.
	 * @param id ID de la categoría a buscar.
	 * @return La respectiva Categoria, o null si no hay alguna categoría
	 * con dicho ID dentro de la BD.
	 */
	public Categoria buscarCategoria(String id) {
		for (Categoria categoria : categorias)
			if ( categoria.getID().equals(id) )
				return categoria;
		return null;
	}

	/**
	 * Actualiza los datos de una categoría dentro de la BD.
	 * @param categoria Categoría cuyos datos serán modificados.
	 * @param linea Cadena con los datos de la categoría, simula
	 * la línea del respectivo archivo CSV.
	 */
	public void setCategoria(Categoria categoria, String linea) {
		String[] datos = linea.split(",");
		categoria.setDatos(datos);
	}

	/**
	 * Elimina una categoría de la BD.
	 * @param categoria Categoría a ser eliminada.
	 */
	public void eliminarCategoria(Categoria categoria) {
		categorias.remove(categoria);
	}

	// DE LA BASE DE DATOS...

	/**
	 * Guarda todos los cambios realizados a la
	 * BD durante la ejecución del programa.
	 */
	public void cerrar() {

		// Guardamos a los clientes...
		clienteCSV.vaciar();
		for (Cliente c : clientes)
			clienteCSV.escribirLinea(c + "\n");
	}

	/**
	 * Lee los archivos CSV para construir la BD.
	 */
	private void leerBD() {

		// Leemos a los clientes...
		for (String linea : clienteCSV.getLineas()) {
			if (linea.equals(""))
				continue;
			String[] datos = linea.split(",");
			Cliente actual = new Cliente(datos);
			clientes.add(actual);
		}

		// Leemos a los proveedores...
		for (String linea : proveedorCSV.getLineas()) {
			if (linea.equals(""))
				continue;
			String[] datos = linea.split(",");
			Proveedor actual = new Proveedor(datos);
			proveedores.add(actual);
		}

		// Leemos a los productos...
		for (String linea : productoCSV.getLineas()) {
			if (linea.equals(""))
				continue;
			String[] datos = linea.split(",");
			Producto actual = new Producto(datos);
			productos.add(actual);
		}

		// Leemos a las categorías...
		for (String linea : categoriaCSV.getLineas()) {
			if (linea.equals(""))
				continue;
			String[] datos = linea.split(",");
			Categoria actual = new Categoria(datos);
			categorias.add(actual);
		}
	}

}