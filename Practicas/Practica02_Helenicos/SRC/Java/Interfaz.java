import java.util.Scanner;
import Entidades.*;

/**
 * Clase encargada de la interacción con
 * el usuario mediante el input y el
 * output a través de la terminal.
 */
public class Interfaz {

	private Scanner scanner;
	private ManejadorBD bd;

	/**
	 * Método constructor de una
	 * Interfaz.
	 */
	public Interfaz() {
		scanner = new Scanner(System.in);
		bd = new ManejadorBD();
	}

	/**
	 * Método encargado del menú
	 * principal del programa.
	 */
	public void menuPrincipal() {
		System.out.println("NIXUT'S DB");
		System.out.println("----------");
		System.out.println("Cargando base de datos...");
		System.out.println("¡Base de datos lista!");
		System.out.println("1) Agregar.");
		System.out.println("2) Consultar.");
		System.out.println("3) Editar.");
		System.out.println("4) Eliminar.");
		System.out.println("5) Salir.");

		switch( getOpcion(1, 5) ) {
			case 1:
				menuAgregar();
				break;
			case 2:
				menuConsultar();
				break;
			case 3:
				menuEditar();
				break;
			case 4:
				menuEliminar();
				break;
			case 5:
				System.out.println("Guardando la base de datos...");
				bd.cerrar();
				break;
		}
	}

	/**
	 * Regresa un número del usuario entre un rango dado.
	 * @param init Número mínimo a pedir.
	 * @param fini Número máximo a pedir.
	 * @return Input numérico del usuario.
	 */
	private int getOpcion(int init, int fini) {
		System.out.println("Escoge una opción:");
		int opcion = scanner.nextInt();
		if (opcion < init || opcion > fini) {
			System.out.println(String.format("Debes escoger una opción entre %d y %d.", init, fini));
			return getOpcion(init, fini);
		}
		scanner.nextLine();
		return opcion;
	}

	/**
	 * Regresa una cadena del usuario.
	 * @return Input alfanumérico del usuario.
	 */
	private String getString() {
		return scanner.nextLine();
	}

	/**
	 * Menú que imprime las entidades de la BD.
	 */
	private void menuEntidades() {
		System.out.println("1) Clientes.");
		System.out.println("2) Proveedores.");
		System.out.println("3) Productos.");
		System.out.println("4) Categorías.");
	}

	/**
	 * Menú para agregar datos a la BD.
	 */
	private void menuAgregar() {
		System.out.println("¿Qué deseas agregar?");
		menuEntidades();
		switch( getOpcion(1, 4) ) {
			case 1:
				System.out.println("Anota la línea de atributos del cliente (como si fuera dentro del archivo .csv):");
				try {
					bd.agregarCliente(getString());
				} catch (IllegalArgumentException iae) {
					System.out.println(iae.getMessage());
					break;
				}
				System.out.println("Cliente agregado con éxito.");
				break;
			case 2:
				System.out.println("Anota la línea de atributos del proveedor (como si fuera dentro del archivo .csv):");
				try {
					bd.agregarProveedor(getString());
				} catch (IllegalArgumentException iae) {
					System.out.println(iae.getMessage());
					break;
				}
				System.out.println("Proveedor agregado con éxito.");
				//bd.consultarProveedores();
				break;
			case 3:
				System.out.println("Anota la línea de atributos del producto (como si fuera dentro del archivo .csv):");
				try {
					bd.agregarProducto(getString());
				} catch (IllegalArgumentException iae) {
					System.out.println(iae.getMessage());
					break;
				}
				System.out.println("Producto agregado con éxito.");
				break;
			case 4:
				System.out.println("Anota la línea de atributos de la categoría (como si fuera dentro del archivo .csv):");
				try {
					bd.agregarCategoria(getString());
				} catch (IllegalArgumentException iae) {
					System.out.println(iae.getMessage());
					break;
				}
				System.out.println("Categoría agregada con éxito.");
				break;
		}
		esperaYRepite();
	}

	/**
	 * Menú para consultar datos de la BD.
	 */
	private void menuConsultar() {
		System.out.println("¿Sobre qué quieres consultar?");
		menuEntidades();
		switch( getOpcion(1, 4) ) {
			case 1:
				System.out.println("1) Un cliente en específico.");
				System.out.println("2) Todos los clientes.");
				switch( getOpcion(1, 2) ) {
					case 1:
						System.out.println("¿Cuál es su CURP?");
						String curp = getString();
						Cliente cliente = bd.buscarCliente(curp);
						if (cliente == null){
							System.out.println("Cliente con CURP " + curp + " no encontrado.");
						} else {
							System.out.println(cliente);
						}
						break;
					case 2:
						bd.consultarClientes();
						break;
				}
				break;
			case 2:
				System.out.println("1) Un proveedor en específico.");
				System.out.println("2) Todos los proveedores.");
				switch( getOpcion(1, 2) ) {
					case 1:
						System.out.println("¿Cuál es su RFC?");
						String rfc = getString();
						Proveedor proveedor = bd.buscarProveedor(rfc);
						if (proveedor == null){
							System.out.println("Proveedor con RFC " + rfc + " no encontrado.");
						} else {
							System.out.println(proveedor);
						}
						break;
					case 2:
						bd.consultarProveedores();
						break;
				}
				break;
			case 3:
				System.out.println("1) Un producto en específico.");
				System.out.println("2) Todos los productos.");
				switch( getOpcion(1, 2) ) {
					case 1:
						System.out.println("¿Cuál es su ID?");
						String id = getString();
						Producto producto = bd.buscarProducto(id);
						if (producto == null){
							System.out.println("Producto con ID " + id + " no encontrado.");
						} else {
							System.out.println(producto);
						}
						break;
					case 2:
						bd.consultarProductos();
						break;
				}
				break;
			case 4:
				System.out.println("1) Una categoría en específico.");
				System.out.println("2) Todas las categorías.");
				switch( getOpcion(1, 2) ) {
					case 1:
						System.out.println("¿Cuál es su ID?");
						String id = getString();
						Categoria categoria = bd.buscarCategoria(id);
						if (categoria == null){
							System.out.println("Categoría con ID " + id + " no encontrada.");
						} else {
							System.out.println(categoria);
						}
						break;
					case 2:
						bd.consultarCategorias();
						break;
				}
				break;
		}
		esperaYRepite();
	}

	/**
	 * Menú para editar datos de la BD.
	 */
	private void menuEditar() {
		System.out.println("¿Qué deseas editar?");
		menuEntidades();
		switch( getOpcion(1, 4) ) {
			case 1:
				System.out.println("Ingresa la CURP del cliente a editar:");
				String curp = getString();
				Cliente cliente = bd.buscarCliente(curp);
				if (cliente == null){
					System.out.println("Cliente con CURP " + curp + " no encontrado.");
				} else {
					System.out.println("Anota la nueva línea de atributos (como si fuera dentro del archivo .csv):");
					try {
						bd.setCliente(cliente, getString());
					} catch (IllegalArgumentException iae) {
						System.out.println(iae.getMessage());
						break;
					}
					System.out.println("Cliente editado con éxito.");
				}
				break;
			case 2:
				System.out.println("Ingresa el RFC del proveedor a editar:");
				String rfc = getString();
				Proveedor proveedor = bd.buscarProveedor(rfc);
				if (proveedor == null){
					System.out.println("Proveedor con RFC " + rfc + " no encontrado.");
				} else {
					System.out.println("Anota la nueva línea de atributos (como si fuera dentro del archivo .csv):");
					try {
						bd.setProveedor(proveedor, getString());
					} catch (IllegalArgumentException iae) {
						System.out.println(iae.getMessage());
						break;
					}
					System.out.println("Proveedor editado con éxito.");
				}
				break;
			case 3:
				System.out.println("Ingresa el ID del producto a editar:");
				String id = getString();
				Producto producto = bd.buscarProducto(id);
				if (producto == null){
					System.out.println("Producto con ID " + id + " no encontrado.");
				} else {
					System.out.println("Anota la nueva línea de atributos (como si fuera dentro del archivo .csv):");
					try {
						bd.setProducto(producto, getString());
					} catch (IllegalArgumentException iae) {
						System.out.println(iae.getMessage());
						break;
					}
					System.out.println("Producto editado con éxito.");
				}
				break;
			case 4:
				System.out.println("Ingresa el ID de la categoría a editar:");
				String idCategoria = getString();
				Categoria categoria = bd.buscarCategoria(idCategoria);
				if (categoria == null){
					System.out.println("Categoría con ID " + idCategoria + " no encontrada.");
				} else {
					System.out.println("Anota la nueva línea de atributos (como si fuera dentro del archivo .csv):");
					try {
						bd.setCategoria(categoria, getString());
					} catch (IllegalArgumentException iae) {
						System.out.println(iae.getMessage());
						break;
					}
					System.out.println("Categoría editada con éxito.");
				}
				break;
		}
		esperaYRepite();
	}

	/**
	 * Menú para eliminar datos de la BD.
	 */
	private void menuEliminar() {
		System.out.println("¿Qué deseas eliminar?");
		menuEntidades();
		switch( getOpcion(1, 4) ) {
			case 1:
				System.out.println("Anota la CURP del cliente a eliminar:");
				String curp = getString();
				Cliente cliente = bd.buscarCliente(curp);
				if (cliente == null){
					System.out.println("Cliente con CURP " + curp + " no encontrado.");
				} else {
					bd.eliminarCliente(cliente);
					System.out.println("Cliente eliminado con éxito.");
				}
				break;
			case 2:
				System.out.println("Anota el RFC del proveedor a eliminar:");
				String rfc = getString();
				Proveedor proveedor = bd.buscarProveedor(rfc);
				if (proveedor == null){
					System.out.println("Proveedor con RFC " + rfc + " no encontrado.");
				} else {
					bd.eliminarProveedor(proveedor);
					System.out.println("Proveedor eliminado con éxito.");
				}
				break;
			case 3:
				System.out.println("Anota el ID del producto a eliminar:");
				String id = getString();
				Producto producto = bd.buscarProducto(id);
				if (producto == null){
					System.out.println("Producto con ID " + id + " no encontrado.");
				} else {
					bd.eliminarProducto(producto);
					System.out.println("Producto eliminado con éxito.");
				}
				break;
			case 4:
				System.out.println("Anota el ID de la categoría a eliminar:");
				String idCategoria = getString();
				Categoria categoria = bd.buscarCategoria(idCategoria);
				if (categoria == null){
					System.out.println("Categoría con ID " + idCategoria + " no encontrada.");
				} else {
					bd.eliminarCategoria(categoria);
					System.out.println("Categoría eliminada con éxito.");
				}
				break;
		}
		esperaYRepite();
	}

	/**
	 * Imprime una instrucción al usuario para que
	 * éste pueda visualizar el resultado de alguna
	 * operación previa antes de volver a imprimir
	 * el menú principal.
	 */
	private void esperaYRepite() {
		System.out.println("Presiona enter para continuar.");
		getString();
		menuPrincipal();
	}

}