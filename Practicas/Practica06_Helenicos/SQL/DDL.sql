/*
Script para la creacion de la Base de Datos
del caso de uso para la empresa Nixut.
*/

--Creacion del esquema...
DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA public;

--Creacion de las entidades...

--SUPERENTIDAD PERSONA.
CREATE TABLE Persona(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	FechaDeNacimiento DATE NOT NULL,
	Genero CHAR(1) NOT NULL CHECK (Genero in ('M','F')),
	Nombre VARCHAR(50) CHECK (Nombre <> ''),
	ApellidoPaterno VARCHAR(50) CHECK (ApellidoPaterno <> ''),
	ApellidoMaterno VARCHAR(50) CHECK (ApellidoMaterno <> ''),
	NumeroDeCalle int CHECK (NumeroDeCalle between 0 and 1000),
	NombreDeCalle VARCHAR(50) CHECK (NombreDeCalle <> ''),
	Municipio VARCHAR(50) CHECK (Municipio <> ''),
	CodigoPostal CHAR(5) CHECK (CodigoPostal SIMILAR TO '[0-9]*'),
	--CodigoPostal int  CHECK (CodigoPostal between 0 and 99999), --Alternativa para CP.
	Estado VARCHAR(50) CHECK (Estado <> ''),
	PRIMARY KEY(CURP)
);

--REPARTIDOR (subentidad de Persona).
CREATE TABLE Repartidor(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Salario int CHECK (Salario > 0),
	Calificacion decimal(4,2) CHECK (Calificacion between 0 and 10),
	HorarioDeEntrada int CHECK (HorarioDeEntrada between 0 and 86400),
	HorarioDeSalida int CHECK (HorarioDeSalida between 0 and 86400),
	FOREIGN KEY(CURP) REFERENCES Persona(CURP) --Referencia a Persona.
);

--CLIENTE (subentidad de Persona).
CREATE TABLE Cliente(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Contraseña VARCHAR(30) CHECK (Contraseña <> ''),
	Correo VARCHAR(30) CHECK(Correo <> '' and Correo like '_%@_%._%'),
	PuntosAcumulados int,
	FOREIGN KEY(CURP) REFERENCES Persona(CURP) --Referencia a Persona.
);

--ENTIDAD PROVEEDOR.
CREATE TABLE Proveedor(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13) UNIQUE,
	RazonSocial VARCHAR(50) CHECK (RazonSocial <> ''),
	NumeroDeCalle int CHECK (NumeroDeCalle between 0 and 1000),
	NombreDeCalle VARCHAR(50) CHECK (NombreDeCalle <> ''),
	Municipio VARCHAR(50) CHECK (Municipio <> ''),
	CodigoPostal CHAR(5) CHECK (CodigoPostal SIMILAR TO '[0-9]*'),
	Estado VARCHAR(50) CHECK (Estado <> ''),
	PRIMARY KEY(RFC)
);

--Telefono de Contacto (multivaluado) de los proveedores.
CREATE TABLE TelefonoProveedor(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13) UNIQUE,
	TelefonoDeContacto CHAR(10) CHECK (TelefonoDeContacto SIMILAR TO '[0-9]*'),
	PRIMARY KEY(RFC, TelefonoDeContacto),
	FOREIGN KEY(RFC) REFERENCES Proveedor(RFC) --Referencia a Proveedor.
);

--ENTIDAD CATEGORIA.
CREATE TABLE Categoria(
	ID VARCHAR(30) NOT NULL UNIQUE,
	Nombre VARCHAR(30) CHECK(Nombre <> ''),
	Descripcion VARCHAR(300) CHECK(Descripcion <> ''),
	PRIMARY KEY(ID)
);

--ENTIDAD PRODUCTO.
CREATE TABLE Producto(
	ID VARCHAR(30) NOT NULL UNIQUE,
	Nombre VARCHAR(50) CHECK(Nombre <> ''),
	Precio decimal(8,2) CHECK (Precio > 0),
	Descripcion VARCHAR(300) CHECK(Descripcion <> ''),
	ImagenAsociada VARCHAR(50) CHECK (ImagenAsociada <> ''),
	Descuento decimal(3,2) CHECK (Descuento between 0 and 1),
	--UnidadesDisp int CHECK (UnidadesDisp between 0 and 99999), --Atributo calculado.
	PRIMARY KEY(ID),
	FOREIGN KEY(ID) REFERENCES Categoria(ID)
);

--ENTIDAD PEDIDO.
CREATE TABLE Pedido(
	Numero int NOT NULL CHECK (Numero between 0 and 999999) UNIQUE,
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	PRIMARY KEY(Numero),
	FOREIGN KEY(CURP) REFERENCES Cliente(CURP) --Referencia a Cliente.
);

--ENTIDAD ENVIO.
CREATE TABLE Envio(
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	Numero int NOT NULL CHECK(Numero between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	PRIMARY KEY(Numero, CURP),
	FOREIGN KEY(CURP) REFERENCES Repartidor(CURP) --Referencia a Repartidor.
);

--NORMAL (subentidad de Envio).
CREATE TABLE Normal(
	--La CURP está en Envio.
	NumeroDeEnvio int NOT NULL CHECK(NumeroDeEnvio between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	TiempoDeDemanda INT NOT NULL CHECK(TiempoDeDemanda > 0),
	PRIMARY KEY(NumeroDeEnvio),
	FOREIGN KEY(NumeroDeEnvio) REFERENCES Envio(Numero) --Referencia a Envio.

);

--EXPRESS (subentidad de Envio).
CREATE TABLE Express(
	--La CURP está en Envio.
	NumeroDeEnvio int NOT NULL CHECK(NumeroDeEnvio between 0 and 999999) UNIQUE,
	FueCompletado BOOLEAN NOT NULL,
	TiempoUsuario INT NOT NULL CHECK(TiempoUsuario > 0),
	Costo INT CHECK(Costo >= 0),
	PRIMARY KEY(NumeroDeEnvio),
	FOREIGN KEY(NumeroDeEnvio) REFERENCES Envio(Numero) --Referencia a Envio.
);

--ENTIDAD TARJETA.
CREATE TABLE Tarjeta(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	Vencimiento DATE NOT NULL,
	NombreDeTitular VARCHAR(60) CHECK (NombreDeTitular <> ''),
	CVV CHAR(3) CHECK (CVV SIMILAR TO '[0-9]*'),
	PRIMARY KEY(NumDeTarjeta),
	CURP CHAR(18) NOT NULL CHECK (CHAR_LENGTH(CURP)=18) UNIQUE,
	FOREIGN KEY(CURP) references Pedido(CURP) --Referencia a Pedido.
);

--CREDITO (subentidad de Tarjeta).
CREATE TABLE Credito(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	FOREIGN KEY(NumDeTarjeta) references Tarjeta(NumDeTarjeta) --Referencia a Tarjeta.
);

--DEBITO (subentidad de Tarjeta).
CREATE TABLE Debito(
	NumDeTarjeta CHAR(16) CHECK (NumDeTarjeta SIMILAR TO '[0-9]*') UNIQUE,
	FOREIGN KEY(NumDeTarjeta) references Tarjeta(NumDeTarjeta) --Referencia a Tarjeta.
);

--RELACION VENDER.
CREATE TABLE Vender(
	RFC CHAR(13) NOT NULL CHECK (CHAR_LENGTH(RFC)=13),
	ID VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY(RFC, ID),
	FOREIGN KEY(RFC) REFERENCES Proveedor(RFC),
	FOREIGN KEY(ID) REFERENCES Producto(ID)
);