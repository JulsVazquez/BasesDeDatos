--1) Por cada facultad cuantas veces ha sido destino en los viajes.
SELECT taxis.facultad_instituto.nombre, COUNT(taxis.facultad_instituto.id_facultad_instituto) esDestino
FROM taxis.destino JOIN taxis.facultad_instituto ON taxis.destino.destino = taxis.facultad_instituto.id_facultad_instituto
GROUP BY taxis.facultad_instituto.nombre
ORDER BY esDestino;

--2) El numero de viajes que realiza cada cliente por mes, año y toda la informacion del cliente.
SELECT EXTRACT(YEAR FROM taxis.viajes.fecha) AÑO, taxis.cliente.id_cliente, taxis.cliente.nombre, 
	   taxis.cliente.paterno, taxis.cliente.materno, taxis.cliente.nacimiento, taxis.cliente.tel_casa, 
	   taxis.cliente.celular, taxis.cliente.mail, taxis.cliente.calle, taxis.cliente.numero, taxis.cliente.ciudad, 
	   taxis.cliente.cp, taxis.facultad_instituto.nombre Facultad_Instituto_Unidad, taxis.cliente.hora_entrada, 
	   taxis.cliente.hora_salida, COUNT(taxis.viajes.id_viaje)
FROM taxis.cliente JOIN taxis.viajes ON taxis.cliente.id_cliente = taxis.viajes.id_cliente JOIN taxis.facultad_instituto
	 ON taxis.facultad_instituto.id_facultad_instituto = taxis.cliente.id_facultad_instituto
GROUP BY AÑO, taxis.cliente.id_cliente, taxis.cliente.nombre, taxis.cliente.paterno, taxis.cliente.materno,
	   taxis.cliente.nacimiento, taxis.cliente.tel_casa, taxis.cliente.celular, taxis.cliente.mail,
	   taxis.cliente.calle, taxis.cliente.numero, taxis.cliente.ciudad, taxis.cliente.cp,  Facultad_Instituto_Unidad,
	   taxis.cliente.hora_entrada, taxis.cliente.hora_salida
ORDER BY AÑO DESC;

--3) Informacion completa de los choferes que tienen 3 multas o mas
SELECT taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno, taxis.chofer.nacimiento,
	   taxis.chofer.calle, taxis.chofer.numero, taxis.chofer.ciudad, taxis.chofer.ciudad, taxis.chofer.cp, 
	   taxis.chofer.licencia, taxis.chofer.fecha_ingreso, taxis.chofer.celular, taxis.chofer.mail, 
	   COUNT(taxis.infracciones.id_infracciones) 
FROM taxis.infracciones JOIN taxis.chofer ON taxis.infracciones.curp_conductor = taxis.chofer.curp
GROUP BY taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno, taxis.chofer.nacimiento,
	   taxis.chofer.calle, taxis.chofer.numero, taxis.chofer.ciudad, taxis.chofer.ciudad, taxis.chofer.cp, 
	   taxis.chofer.licencia, taxis.chofer.fecha_ingreso, taxis.chofer.celular, taxis.chofer.mail
HAVING COUNT(taxis.infracciones.id_infracciones) >= 3;

--4) El numero de viajes que realiza un chofer por mes para cada año.
SELECT EXTRACT(YEAR FROM taxis.viajes.fecha) AÑO, EXTRACT(MONTH FROM taxis.viajes.fecha) MES, taxis.chofer.curp,
	   taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno, COUNT(taxis.viajes.id_viaje) viajes_realizados
FROM taxis.viajes JOIN taxis.ganar ON taxis.viajes.id_viaje = taxis.ganar.id_viaje JOIN 
	 taxis.chofer ON taxis.chofer.curp = taxis.ganar.curp_chofer
GROUP BY AÑO, MES, taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno
ORDER BY AÑO DESC;

--5) Informacion sobre cuantas multas tiene cada auto
SELECT taxis.vehiculo_activo.num_economico, COUNT(taxis.infracciones.id_infracciones) num_Infracciones
FROM taxis.infracciones JOIN taxis.vehiculo_activo 
	 ON taxis.infracciones.num_economico = taxis.vehiculo_activo.num_economico
GROUP BY taxis.vehiculo_activo.num_economico
ORDER BY num_infracciones ASC;

--6) El numero de vehiculos que tiene cada dueño ya sea que esten en la flotilla o ya no esten.
SELECT Registros.curp Dueño, COUNT(Registros.num_economico) Autos
FROM ((SELECT taxis.dueño.curp, taxis.vehiculo_activo.num_economico
	  FROM taxis.dueño JOIN taxis.vehiculo_activo ON taxis.dueño.curp = taxis.vehiculo_activo.curp_dueño)
	  UNION
	  (SELECT taxis.dueño.curp, taxis.vehiculo_inactivo.num_economico
	  FROM taxis.dueño JOIN taxis.vehiculo_inactivo ON taxis.dueño.curp = taxis.vehiculo_inactivo.curp_dueño))Registros
GROUP BY Dueño
ORDER BY Autos;
	   
--7) Por cada faccultad el numero de veces que es origen de un viaje
SELECT taxis.facultad_instituto.nombre, COUNT(taxis.facultad_instituto.id_facultad_instituto) es_origen
FROM taxis.origen JOIN taxis.facultad_instituto ON taxis.origen.origen = taxis.facultad_instituto.id_facultad_instituto
GROUP BY taxis.facultad_instituto.nombre
ORDER BY es_origen;
	   
--8) Tenemos la informacion de las aseguradoras, por cada aseguradora cuantos autos tienen un seguro con esta
--aseguradora y la cobertura que tiene.
SELECT taxis.aseguradora.rfc, taxis.aseguradora.razon_social, COUNT(taxis.vehiculo_activo.num_economico) vehiculos
FROM  taxis.aseguradora JOIN taxis.tener ON taxis.aseguradora.rfc = taxis.tener.rfc JOIN
	  taxis.vehiculo_activo ON taxis.tener.num_economico = taxis.vehiculo_activo.num_economico
GROUP BY taxis.aseguradora.rfc, taxis.aseguradora.razon_social
ORDER BY vehiculos;
	   
--9) Ganancia promedio por año para cada chofer	   
SELECT EXTRACT(YEAR FROM taxis.viajes.fecha) AÑO,taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, 
	   taxis.chofer.materno, AVG(taxis.ganar.ganancia) Ganancia_anual
FROM taxis.ganar JOIN taxis.chofer ON taxis.ganar.curp_chofer = taxis.chofer.curp JOIN taxis.viajes ON
	 taxis.ganar.id_viaje = taxis.viajes.id_viaje
GROUP BY AÑO, taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno
ORDER BY AÑO;	  
	   
--10) Los clientes que tuvieron mas de 5 viajes en el segundo trimestre del año actual
SELECT taxis.cliente.id_cliente, taxis.cliente.nombre, taxis.cliente.paterno,COUNT(taxis.viajes.id_viaje) VIAJES
FROM taxis.viajes JOIN taxis.cliente ON taxis.cliente.id_cliente = taxis.viajes.id_cliente
WHERE EXTRACT(QUARTER FROM taxis.viajes.fecha) = 2 AND
	  EXTRACT(YEAR FROM taxis.viajes.fecha) = EXTRACT(YEAR FROM NOW())													   
GROUP BY taxis.cliente.id_cliente, taxis.cliente.nombre, taxis.cliente.paterno
HAVING COUNT(taxis.viajes.id_viaje) > 5													  
ORDER BY VIAJES;	
													  
--11) La antiguedad de cada chofer ordenadna de mayor a menor
SELECT taxis.chofer.curp, taxis.chofer.nombre, taxis.chofer.paterno, taxis.chofer.materno, 
	   EXTRACT(YEAR FROM NOW()) - EXTRACT(YEAR FROM taxis.chofer.fecha_ingreso) Antiguedad
FROM taxis.chofer
ORDER BY Antiguedad DESC;	
			   
--12) Los choferes que viven en una ciudad distinta que los dueños de los vehiculos que manejan.
SELECT taxis.chofer.curp
FROM taxis.chofer JOIN taxis.conducir ON taxis.chofer.curp = taxis.conducir.curp_conductor JOIN 
	 taxis.vehiculo_activo ON taxis.vehiculo_activo.num_economico = taxis.conducir.num_economico JOIN
	 taxis.dueño ON taxis.vehiculo_activo.curp_dueño = taxis.dueño.curp
WHERE taxis.chofer.ciudad != taxis.dueño.ciudad AND taxis.chofer.curp != taxis.dueño.curp;	
			   
--13) Por cada cliente los viajes que hizo donde comenzo y donde termino
SELECT origen.id_cliente, origen.id_viaje, origen.origen, destino.destino
FROM			   
(SELECT taxis.cliente.id_cliente, taxis.viajes.id_viaje, taxis.facultad_instituto.nombre Origen
FROM taxis.cliente JOIN taxis.viajes ON taxis.cliente.id_cliente = taxis.viajes.id_cliente JOIN taxis.origen
	 ON taxis.viajes.id_viaje = taxis.origen.id_viaje JOIN taxis.facultad_instituto ON 
	 taxis.origen.origen = taxis.facultad_instituto.id_facultad_instituto) Origen JOIN			   
(SELECT taxis.cliente.id_cliente, taxis.viajes.id_viaje, taxis.facultad_instituto.nombre Destino
FROM taxis.cliente JOIN taxis.viajes ON taxis.cliente.id_cliente = taxis.viajes.id_cliente JOIN taxis.destino
	 ON taxis.viajes.id_viaje = taxis.destino.id_viaje JOIN taxis.facultad_instituto ON 
	 taxis.destino.destino = taxis.facultad_instituto.id_facultad_instituto)Destino 
	 ON origen.id_cliente = Destino.id_cliente AND  origen.id_viaje = destino.id_viaje
ORDER BY origen.id_cliente;			   
			  
--14) El numero de viajes que ha realizado cada vehiculo
SELECT taxis.vehiculo_activo.num_economico, COUNT(taxis.viajes.id_viaje) Viajes
FROM taxis.viajes JOIN taxis.vehiculo_activo ON taxis.viajes.num_economico = taxis.vehiculo_activo.num_economico
GROUP BY taxis.vehiculo_activo.num_economico
ORDER BY Viajes;	
			   
--15) Cliente y choferes que los han atendidio con su foto.
SELECT taxis.cliente.nombre, taxis.chofer.nombre, taxis.chofer.materno, taxis.chofer.paterno, taxis.chofer.fotografia
FROM taxis.viajes JOIN taxis.cliente ON taxis.viajes.id_cliente = taxis.cliente.id_cliente JOIN taxis.conducir 
			ON taxis.viajes.num_economico = taxis.conducir.num_economico JOIN taxis.chofer
			ON taxis.conducir.curp_conductor = taxis.chofer.curp;
