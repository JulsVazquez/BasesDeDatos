CREATE SCHEMA Taxis;

/*Creación de la tabla Facultad_Instituto*/
CREATE TABLE Taxis.Facultad_Instituto(
id_facultad_instituto integer NOT NULL,
nombre text NOT NULL,
CONSTRAINT Facultad_Instituto_pkey PRIMARY KEY (id_facultad_instituto)
);

/*Creación de la tabla Cliente*/
CREATE TABLE Taxis.Cliente(
id_cliente integer NOT NULL,
tipo_cliente text NOT NULL CHECK (UPPER(tipo_cliente) IN ('ALUMNO','ACADEMICO','TRABAJADOR')),
hora_salida time,
hora_entrada time,
nacimiento date,
id_facultad_instituto integer,
mail character varying (50) CHECK (mail ~ '^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'),
fotografia text,
celular character varying(10) CHECK (celular ~ '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
tel_casa character varying(8) CHECK (tel_casa ~ '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
cp integer,
ciudad text,
numero integer,
calle text,
materno text,
paterno text,
nombre text,
CONSTRAINT id_cliente_pkey PRIMARY KEY (id_cliente),
CONSTRAINT cliente_fk FOREIGN KEY (id_facultad_instituto) REFERENCES Taxis.Facultad_Instituto (id_facultad_instituto)
);

/*Creación de la tabla Dueño*/
CREATE TABLE Taxis.Dueño(
curp character varying (20) NOT NULL,
nombre text,
paterno text,
materno text,
calle character varying (20),
numero integer,
ciudad text,
cp integer,
licencia integer, /*Es necesario?*/
fecha_ingreso date,
fotografia text,
celular character varying(10) CHECK (celular ~ '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
mail character varying (50) CHECK (mail ~ '^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'),
nacimiento date,
rfc character varying (11) NOT NULL,
CONSTRAINT id_dueño PRIMARY KEY (curp)
);								  

/*Creación de la tabla Chofer*/
CREATE TABLE Taxis.Chofer(
curp character varying (20) NOT NULL,
nombre text,
paterno text,
materno text,
calle text,
numero integer,
ciudad text,
cp integer,
licencia integer NOT NULL,
fecha_ingreso date,
fotografia text,
celular character varying(10) NOT NULL CHECK (celular ~ '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
mail character varying (50) CHECK (mail ~ '^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$'),
nacimiento date,
CONSTRAINT id_chofer_pkey PRIMARY KEY (curp)
);

/*Creación de la tabla Vehiculos_inactivos*/
CREATE TABLE Taxis.Vehiculo_inactivo(
curp_dueño character  varying (20) NOT NULL,
num_economico integer NOT NULL,
llanta_refraccion boolean,
combustible text CHECK (UPPER(combustible) IN ('GASOLINA','HIBRIDO')),
año integer,
puertas integer,
modelo character varying (30),
pasajeros integer,
tanque integer,
marca text,
cilindros integer, 
transmision text CHECK (UPPER(transmision) IN ('MANUAL','AUTOMATICA')),
razon text,
CONSTRAINT id_V_inactivo PRIMARY KEY (num_economico),
CONSTRAINT V_inactivo_fk FOREIGN KEY (curp_dueño)
    REFERENCES Taxis.Dueño (curp)
);

/*Creación de la tabla Vehiculo_activo*/
CREATE TABLE Taxis.Vehiculo_activo(
curp_dueño character varying (20) NOT NULL,
num_economico integer NOT NULL,
llanta_refraccion boolean,
combustible text CHECK (UPPER(combustible) IN ('GASOLINA','HIBRIDO')),
año integer,
puertas integer,
modelo character varying (30),
pasajeros integer,
tanque integer,
marca text,
cilindros integer,
transmision text CHECK (UPPER(transmision) IN ('MANUAL','AUTOMATICA')),
CONSTRAINT id_V_activo PRIMARY KEY (num_economico),
CONSTRAINT V_activo_fk FOREIGN KEY (curp_dueño)
    REFERENCES Taxis.Dueño (curp)
);

/*Creación de la tabla Infracciones*/
CREATE TABLE Taxis.Infracciones(
id_infracciones integer NOT NULL,
num_economico integer NOT NULL,
ubicacion character varying (100),
agente_trancito integer,
hora time,
fecha date,
costo integer,
descripcion text,
curp_conductor character varying (20) NOT NULL,
CONSTRAINT id_infracciones PRIMARY KEY (id_infracciones),
CONSTRAINT infracciones_V_fk FOREIGN KEY (num_economico)
    REFERENCES Taxis.Vehiculo_activo (num_economico),
CONSTRAINT infracciones_C_fk FOREIGN KEY (curp_conductor)
    REFERENCES Taxis.Chofer (curp)
);

/*Creación de la tabla Conducir*/
CREATE TABLE Taxis.Conducir(
curp_conductor character varying (20) NOT NULL,
num_economico integer NOT NULL,
CONSTRAINT id_conducir PRIMARY KEY (curp_conductor,num_economico),
CONSTRAINT conducir_V_fk FOREIGN KEY (num_economico)
    REFERENCES Taxis.Vehiculo_activo (num_economico),
CONSTRAINT conducir_C_fk FOREIGN KEY (curp_conductor)
    REFERENCES Taxis.Chofer (curp)
);

/*Creación de la tabla Viajes*/
CREATE TABLE Taxis.Viajes(
id_viaje integer NOT NULL,
id_cliente integer NOT NULL,
num_economico integer NOT NULL,
fecha date,
tiempo time,
distancia float, 
pasajeros integer,
costo float,
CONSTRAINT id_viajes PRIMARY KEY (id_viaje),
CONSTRAINT viajes_V_fk FOREIGN KEY (num_economico)
    REFERENCES Taxis.Vehiculo_activo (num_economico),
CONSTRAINT viajes_C_fk FOREIGN KEY (id_cliente)
    REFERENCES Taxis.Cliente (id_cliente)
);

/*Creación de la tabla Ganar*/
CREATE TABLE Taxis.Ganar(
id_viaje integer NOT NULL,
curp_chofer character varying (20) NOT NULL,
ganancia integer,
CONSTRAINT id_ganar PRIMARY KEY (id_viaje, curp_chofer),
CONSTRAINT ganar_V_fk FOREIGN KEY (id_viaje)
    REFERENCES Taxis.Viajes (id_viaje),
CONSTRAINT ganar_C_fk FOREIGN KEY (curp_chofer)
    REFERENCES Taxis.Chofer (curp)
);

/*Creación de la tabla Destino*/
CREATE TABLE Taxis.Destino(
id_viaje integer NOT NULL,
destino integer NOT NULL,
CONSTRAINT id_destino PRIMARY KEY (id_viaje, destino),
CONSTRAINT destino_fk FOREIGN KEY (id_viaje) REFERENCES Taxis.Viajes (id_viaje),
CONSTRAINT destino_lugar_fk FOREIGN KEY (destino) REFERENCES Taxis.Facultad_Instituto(id_facultad_instituto)
);

/*Creación de la tabla Aseguradora*/
CREATE TABLE Taxis.Aseguradora(
rfc character varying (11) NOT NULL,
razon_social text,
calle character varying (30),
numero integer,
ciudad text,
cp integer,
CONSTRAINT id_aseguradora PRIMARY KEY (rfc)
);

/*Creación de la tabla Tener*/
CREATE TABLE Taxis.Tener(
num_economico integer NOT NULL,
rfc character varying (11) NOT NULL,
cobertura text,
CONSTRAINT id_tener PRIMARY KEY (rfc,num_economico),
CONSTRAINT tener_A_fk FOREIGN KEY (rfc)
    REFERENCES Taxis.Aseguradora (rfc),
CONSTRAINT tener_V_fk FOREIGN KEY (num_economico)
    REFERENCES Taxis.Vehiculo_activo (num_economico)
);

/*Creación de la tabla Origen*/
CREATE TABLE Taxis.Origen(
id_viaje integer NOT NULL,
origen  integer NOT NULL,
CONSTRAINT id_origen PRIMARY KEY (id_viaje,origen),
CONSTRAINT origen_fk FOREIGN KEY (id_viaje) REFERENCES Taxis.Viajes (id_viaje),
CONSTRAINT origen_lugar_fk FOREIGN KEY (origen) REFERENCES Taxis.Facultad_Instituto(id_facultad_instituto)
);
