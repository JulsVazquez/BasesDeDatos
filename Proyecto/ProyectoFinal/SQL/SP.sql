---------------Procedimientos para agregar ------------------------------------
/*1.Procedimiento que agrega una nueva aseguradora, a la tabla aseguradora*/
CREATE OR REPLACE FUNCTION taxis.sp_aseguradora(
rfc character varying(11),
razon_social text,
calle character varying(30),
numero integer,
ciudad text,
cp integer)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."aseguradora" (rfc,razon_social,calle,numero,ciudad,cp)
VALUES ($1, $2,$3, $4, $5, $6);
return;
END; 
$$
LANGUAGE plpgsql;

/*2.Procedimiento que agrega un nuevo chofer, a la tabla chofer*/
CREATE OR REPLACE FUNCTION taxis.sp_chofer(
curp character varying(20),
nombre text,
paterno text,
materno text,
numero integer,
ciudad text,
cp integer,
licencia integer,
fecha_ingreso text,
fotografia text,
celular character varying(10),
mail character varying(50),
nacimiento date)
RETURNS void AS
$$
BEGIN
if (nombre ~ '(0|1|2|3|4|5|6|7|8|9)' OR nombre ~ '\W') then
    RAISE EXCEPTION 'El nombre es invalido';
elsif(paterno ~ '(0|1|2|3|4|5|6|7|8|9)' OR paterno ~ '\W') then
    RAISE EXCEPTION 'El apellido paterno es invalido';
elsif(materno ~ '(0|1|2|3|4|5|6|7|8|9)' OR materno ~ '\W') then
    RAISE EXCEPTION 'El apelllido materno es invalido';
end if;
INSERT INTO "taxis"."chofer"(curp,nombre,paterno,materno,numero,ciudad,cp,licencia,fecha_ingreso,fotografia,celular,mail,nacimiento)
values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);
return;
END;
$$
LANGUAGE plpgsql;

/*3.Procedimiento que agrega un nuevo cliente, a la tabla cliente*/
CREATE OR REPLACE FUNCTION taxis.sp_cliente(
id_cliente integer,
tipo_cliente text,
hora_salida time without time zone,
hora_entrada time without time zone,
nacimiento date,
id_facultad_instituto integer,
mail character varying(50),
fotografia text,
celular character varying(10),
tel_casa character varying(8),
cp integer,
ciudad text,
numero integer,
calle text,
materno text,
paterno text,
nombre text)
RETURNS void AS
$$
BEGIN
if (nombre ~ '(0|1|2|3|4|5|6|7|8|9)' OR nombre ~ '\W') then
    RAISE EXCEPTION 'El nombre es invalido';
elsif(paterno ~ '(0|1|2|3|4|5|6|7|8|9)' OR paterno ~ '\W') then
    RAISE EXCEPTION 'El apellido paterno es invalido';
elsif(materno ~ '(0|1|2|3|4|5|6|7|8|9)' OR materno ~ '\W') then
    RAISE EXCEPTION 'El apelllido materno es invalido';
end if;
INSERT INTO "taxis"."cliente"(id_cliente,tipo_cliente,hora_salida,hora_entrada,naimiento,id_facultad_instituto,
mail,fotografia,celular,tel_casa,cp,ciudad,numero,calle,materno,paterno,nombre)
values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17);
return;
END;
$$
LANGUAGE plpgsql;

/*4.Procedimiento que establece cual es el vehiculo que conduce el conductor, y lo agrega a la tabla conducir*/
CREATE OR REPLACE FUNCTION taxis.sp_conducir(
curp_conductor character varying(20),
num_economico integer)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."conducir"(curp_conductor,num_economico)
values($1,$2);
return;
END;
$$
LANGUAGE plpgsql;

/*5.Procedimiento que establece el destino de un viaje y lo agrega a la tabla destino*/
CREATE OR REPLACE FUNCTION taxis.sp_destino(
id_viaje integer,
destino integer)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."destino"(id_viaje,destino)
values($1,$2);
return;
END;
$$
LANGUAGE plpgsql;

/*6.Procedimiento que agrega un nuevo dueño, a la tabla dueño*/
CREATE OR REPLACE FUNCTION taxis.sp_dueño(
curp character varying(20),
nombre text,
paterno text,
materno text, 
calle character varying(20),
numero integer,
ciudad text,
cp integer,
licencia integer,
fecha_ingreso date,
fotografia text,
celular character varying(10),
mail character varying(50),
nacimiento date,
rfc character varying(11))
RETURNS void AS
$$
BEGIN
if (nombre ~ '(0|1|2|3|4|5|6|7|8|9)' OR nombre ~ '\W') then
    RAISE EXCEPTION 'El nombre es invalido';
elsif(paterno ~ '(0|1|2|3|4|5|6|7|8|9)' OR paterno ~ '\W') then
    RAISE EXCEPTION 'El apellido paterno es invalido';
elsif(materno ~ '(0|1|2|3|4|5|6|7|8|9)' OR materno ~ '\W') then
    RAISE EXCEPTION 'El apelllido materno es invalido';
end if;
INSERT INTO "taxis"."dueño"(curp,nombre,paterno,materno,calle,numero,ciudad,cp,licencia,fecha_ingreso,fotografia,
celular,mail,nacimiento,rfc)
values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15);
return;
END;
$$
LANGUAGE plpgsql;

/*7.Procedimiento que agrega una nueva facultad o instituto a la tabla facultad_instituto*/
CREATE OR REPLACE FUNCTION taxis.sp_facultad_instituto(
id_facultad_instituto integer,
nombre text)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."facultad_instituto"(id_facultad_instituto,nombre)
values($1,$2);
return;
END;
$$
LANGUAGE plpgsql;

/*8.Procedimiento que agrega la ganancia del viaje a la tabla ganar*/
CREATE OR REPLACE FUNCTION taxis.sp_ganar(
id_viaje integer,
curp_chofer character varying(20),
ganancia integer)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."ganar"(id_viaje,cupr_chofer,ganancia)
values($1,$2,$3);
return;
END;
$$
LANGUAGE plpgsql;

/*9.Procedimiento que agrega una infracción a la tabla infracciones.*/
CREATE OR REPLACE FUNCTION taxis.sp_infracciones(
id_infracciones integer,
num_economico integer,
ubicacion character varying(100),
agente_trancito integer,
hora time without time zone,
fecha date,
costo integer,
descripcion text,
curp_conductor character varying(20))
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."infracciones"(id_infracciones,num_economico,ubicacion,agente_trancito,hora,fecha,costo,descripcion,curp_conductor)
values($1,$2,$3,$4,$5,$6,$7,$8,$9);
return;
END;
$$
LANGUAGE plpgsql;
/*Procedimiento que le agrega un origen a algun viaje y lo agrega a la tabla origen*/
CREATE OR REPLACE FUNCTION taxis.sp_origen(
id_infracciones integer,
num_economico integer,
ubicacion character varying(100),
agente_trancito integer,
hora time without time zone,
fecha date,
costo integer,
descripcion text,
curp_conductor character varying(20))
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."origen"(id_infracciones,num_economico,ubicacion,agente_trancito,hora,fecha,costo,descripcion,curp_conductor)
values($1,$2,$3,$4,$5,$6,$7,$8,$9);
return;
END;
$$
LANGUAGE plpgsql;
/*11. Procedimiento agrega una aseguradora a un vehiculo, y lo agrega en la tabla tener*/
CREATE OR REPLACE FUNCTION taxis.sp_tener(
num_economico integer,
rfc character varying(11),
cobertura text)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."tener"(num_economico,rfc,cobertura)
values($1,$2,$3);
return;
END;
$$
LANGUAGE plpgsql;

/*12.Procedimiento que agrega un vehiculo que esta en funcionamiento, a la tabla vehiculo_activo*/
CREATE OR REPLACE FUNCTION taxis.sp_vehiculo_activo(
curp_dueño character varying (20),
num_economico integer,
llanta_refraccion boolean,
combustible text,
año integer,
puertas integer,
modelo character varying(30),
pasajeros integer,
tanque integer,
marca text,
cilindros integer,
transmision text)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."vehiculo_activo"(curp_dueño,num_economico,llanta_refraccion,combustible,año,puertas,
modelo,pasajeros,tanque,marca,cilindros,transmision)
values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);
return;
END;
$$
LANGUAGE plpgsql;

/*13.Procedimiento que agrega un vehiculo que no esta en uso, a la tabla vehiculo_inactivo*/
CREATE OR REPLACE FUNCTION taxis.sp_vehiculo_inactivo(
curp_dueño character varying (20),
num_economico integer,
llanta_refraccion boolean,
combustible text,
año integer,
puertas integer,
modelo character varying(30),
pasajeros integer,
tanque integer,
marca text,
cilindros integer,
transmision text,
razon text)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."vehiculo_inactivo"(curp_dueño,num_economico,llanta_refraccion,combustible,año,puertas,
modelo,pasajeros,tanque,marca,cilindros,transmision,razon)
values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);
return;
END;
$$
LANGUAGE plpgsql;

/*14.Procedimiento que agrega un nuevo viaje a la tabla viaje*/
CREATE OR REPLACE FUNCTION taxis.sp_viajes(
id_viaje integer,
id_cliente integer,
num_economico integer,
fecha date,
tiempo time without time zone,
distancia double precision,
pasajeros integer,
costo double precision)
RETURNS void AS
$$
BEGIN
INSERT INTO "taxis"."viajes"(id_viaje,id_cliente,num_economico,fecha,tiempo,distancia,pasajeros,costo)
values($1,$2,$3,$4,$5,$6,$7,$8);
return;
END;
$$
LANGUAGE plpgsql;


-----------------------Procedimientos para eliminar-------------------------------
/*1.Procedimiento que se encarga de eliminar una aseguradora a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_eliminar_aseguradora(
rfc character varying(11))
RETURNS VOID AS
$$
BEGIN
DELETE FROM "taxis"."tener" WHERE "taxis"."tener".rfc = $1;
DELETE FROM "taxis"."aseguradora" WHERE "taxis"."aseguradora".rfc = $1;
return;
END;
$$
LANGUAGE PLPGSQL;

/*2.Procedimiento que se encarga de eliminar a un chofer a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_eliminar_chofer(
curp character varying(20))
RETURNS VOID AS
$$
BEGIN 
DELETE FROM "taxis"."conducir" WHERE "taxis"."conducir".curp_conductor = $1;
DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".curp_chofer = $1;
DELETE FROM "taxis"."infracciones" WHERE "taxis"."infracciones".curp_conductor = $1;
DELETE FROM "taxis"."chofer" WHERE "taxis"."chofer".curp = $1;
return;
END;
$$
LANGUAGE PLPGSQL;

/*3.Procedimiento que se encarga de eliminar a un cliente a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_eliminar_cliente(
id_cliente int)
RETURNS VOID AS
$$
DECLARE
aux int;
BEGIN
aux := 0;
WHILE aux is not null LOOP
	SELECT INTO aux id_viaje
	FROM "taxis"."viajes"
	WHERE "taxis"."viajes".id_cliente = $1;
	DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".id_viaje = aux;
	DELETE FROM "taxis"."origen" WHERE "taxis"."origen".id_viaje = aux;
	DELETE FROM "taxis"."destino" WHERE "taxis"."destino".id_viaje = aux;
	DELETE FROM "taxis"."viajes" WHERE "taxis"."viajes".id_viaje = aux;
END LOOP;
DELETE FROM "taxis"."cliente" WHERE "taxis"."cliente".id_cliente = $1;
return;
END;
$$
LANGUAGE PLPGSQL;


/*4.Procedimiento que se encarga de eliminar a un dueño a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_eliminar_dueño(
curp character varying(20))
RETURNS  VOID AS
$$
DECLARE 
aux int;
aux1 int;
aux2 int;
BEGIN
aux := 0;
aux1 := 0;
aux2 := 0;
WHILE aux1 is not null LOOP
SELECT INTO aux1 num_economico
FROM "taxis"."vehiculo_activo"
WHERE "taxis"."vehiculo_activo".curp_dueño = $1;
	WHILE aux is not null LOOP
		SELECT INTO aux id_viaje
		FROM "taxis"."viajes"
		WHERE "taxis"."viajes".num_economico = aux1;
		DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".id_viaje = aux;
		DELETE FROM "taxis"."origen" WHERE "taxis"."origen".id_viaje = aux;
		DELETE FROM "taxis"."destino" WHERE "taxis"."destino".id_viaje = aux;
		DELETE FROM "taxis"."viajes" WHERE "taxis"."viajes".id_viaje = aux;
	END LOOP;
	aux = 0;
	DELETE FROM "taxis"."conducir" WHERE "taxis"."conducir".num_economico = aux1;
	DELETE FROM "taxis"."tener" WHERE "taxis"."tener".num_economico = aux1;
	DELETE FROM "taxis"."infracciones" WHERE "taxis"."infracciones".num_economico = aux1;
	DELETE FROM "taxis"."vehiculo_activo" WHERE "taxis"."vehiculo_activo".num_economico = aux1;
END LOOP;
WHILE aux2 is not null LOOP
	SELECT INTO aux2 num_economico
	FROM "taxis"."vehiculo_inactivo"
	WHERE "taxis"."vehiculo_inactivo".curp_dueño = $1;
	DELETE FROM "taxis"."vehiculo_inactivo" WHERE "taxis"."vehiculo_inactivo".num_economico = aux2;
END LOOP;
DELETE FROM "taxis"."dueño" WHERE "taxis"."dueño".curp = $1;
return;
END;
$$
LANGUAGE PLPGSQL;
/*5. Procedimiento que elimina una facultad o instituto a partir de su llave */
CREATE OR REPLACE FUNCTION taxis.sp_elimina_facultad_instituto(
id_facultad_instituto int)
RETURNS VOID AS
$$
DECLARE 
aux int;
aux1 int;
BEGIN
aux := 0;
aux1 := 0;
WHILE aux is not null LOOP
	SELECT INTO aux id_cliente
	FROM "taxis"."cliente"
	WHERE "taxis"."cliente".id_facultad_instituto = $1;
		WHILE aux1 is not null LOOP
		SELECT INTO aux1 id_viaje
		FROM "taxis"."viajes"
		WHERE "taxis"."viajes".id_cliente = aux;
		DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".id_viaje = aux1;
		DELETE FROM "taxis"."origen" WHERE "taxis"."origen".id_viaje = aux1;
		DELETE FROM "taxis"."destino" WHERE "taxis"."destino".id_viaje = aux1;
		DELETE FROM "taxis"."viajes" WHERE "taxis"."viajes".id_viaje = aux1;
	END LOOP;
	aux1 = 0;
	DELETE FROM "taxis"."origen" WHERE "taxis"."origen".origen = $1;
	DELETE FROM "taxis"."cliente" WHERE "taxis"."cliente".id_cliente = aux;
END LOOP;
DELETE FROM "taxis"."facultad_instituto" WHERE "taxis"."facultad_instituto".id_facultad_instituto = $1;
return;
END;
$$
LANGUAGE PLPGSQL;

/*6.Procedimiento que elimina una infracción a partir de la llave*/
CREATE OR REPLACE FUNCTION taxis.sp_elimina_infraccion(
id_infracciones int)
RETURNS VOID AS
$$
BEGIN
DELETE FROM "taxis"."infracciones" WHERE  "taxis"."infracciones".id_infracciones = $1;
END;
$$
LANGUAGE PLPGSQL;

/*7.Procedimiento que elimina un vehiculo_activo a partir de su llave */
CREATE OR REPLACE FUNCTION taxis.sp_elimina_vehiculo_activo(
num_economico integer)
RETURNS VOID AS
$$
DECLARE 
aux int;
BEGIN
aux := 0;
WHILE aux is not null LOOP
	SELECT INTO aux id_viaje
	FROM "taxis"."viajes"
	WHERE "taxis"."viajes".num_economico = $1;
	DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".id_viaje = aux;
	DELETE FROM "taxis"."origen" WHERE "taxis"."origen".id_viaje = aux;
	DELETE FROM "taxis"."destino" WHERE "taxis"."destino".id_viaje = aux;
	DELETE FROM "taxis"."viajes" WHERE "taxis"."viajes".id_viaje = aux;
END LOOP;
DELETE FROM "taxis"."conducir" WHERE "taxis"."conducir".num_economico = $1;
DELETE FROM "taxis"."tener" WHERE "taxis"."tener".num_economico = $1;
DELETE FROM "taxis"."infracciones" WHERE "taxis"."infracciones".num_economico = $1;
DELETE FROM "taxis"."vehiculo_activo" WHERE "taxis"."vehiculo_activo".num_economico = $1;
END;
$$
LANGUAGE PLPGSQL;

/*8.Procedimiento que elimina un vehiculo inactivo a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_elimina_vehiculo_inactivo(
num_economico integer)
RETURNS VOID AS
$$
BEGIN
DELETE FROM "taxis"."vehiculo_inactivo" WHERE "taxis"."vehiculo_inactivo".num_economico = $1;
END;
$$
LANGUAGE PLPGSQL;

/*9. Procedimiento que elimina un viaje a partir de su llave*/
CREATE OR REPLACE FUNCTION taxis.sp_elimina_viaje(
id_viaje integer)
RETURNS VOID AS
$$
BEGIN
DELETE FROM "taxis"."ganar" WHERE "taxis"."ganar".id_viaje = $1;
DELETE FROM "taxis"."origen" WHERE "taxis"."origen".id_viaje = $1;
DELETE FROM "taxis"."destino" WHERE "taxis"."destino".id_viaje = $1;
DELETE FROM "taxis"."viajes" WHERE "taxis"."viajes".id_viaje = $1;
END;
$$
LANGUAGE PLPGSQL;

/*9. Funcion que calcula la ganancia de un chofer por el viaje que realizo*/
CREATE or REPLACE function ganancia(id_viaje integer) returns float as
$$
DECLARE
auxFor integer;
auxFor2 integer;
ValorOriginal float;
desPasajeros float;
d integer;
desTipo float;
BEGIN
/*Ver que todos los destinos estan en CU*/
FOR auxFor IN 
SELECT destino FROM Destino WHERE id_viaje = $1
LOOP
if(auxFor ~ '(1|3|4|5|6|7|13|14|15|16|18|19|20|21|25|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103)') then
/*Si lo estan guarda como valor tentativo 15, ya que eso se cobra si el viaje solo es en CU*/
valorOriginal:= 15;
else 
/*En otro caso tu valor tentativo es 15 (banderazo) mas la distancia por 8 que es lo que vale cada kilometro*/
valorOriginal:= 15 + ((SELECT distancia FROM Viaje WHERE id_viaje = $1)*8);
end if;
END LOOP;
/*LO MISMO PARA EL ORIGEN*/
FOR auxFor2 IN 
SELECT origen FROM Origen WHERE id_viaje = $1
LOOP
if(auxFor2 ~ '(1|3|4|5|6|7|13|14|15|16|18|19|20|21|25|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103)' AND t ==15) then
valorOriginal:=15;
else 
valorOriginal := 15 + ((SELECT distancia FROM Viaje WHERE id_viaje = $1)*8);
end if;
END LOOP;

/*Vamos a guaradar en desPasajeros el descuento por numero de pasajeros*/
if((Select pasajeros FROM Viaje WHERE id_viaje = $1) > 1) then
desPasajeros := (Select pasajeros FROM Viaje WHERE id_viaje = $1) * 0.1 * valorOriginal;
else 
desPasajeros := 0;
end if;

d := (SELECT id_cliente FROM Viaje WHERE id_viaje = $1);
/*Vamos a guaradar en C el descuento por ser estudiante o academico*/
if((Select tipo_cliente FROM Cliente WHERE id_cliente = d) ~ 'ALUMNO') then
desTipo := valorOriginal * 0.15;
else 
desTipo := valorOriginal * 0.10;
end if;
return valorOriginal - desPasajeros -desTipo;

END;
$$
LANGUAGE plpgsql;
